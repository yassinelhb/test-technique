const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MarketSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
  },
  image: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Market", MarketSchema, "markets");
