var express = require("express");
var router = express.Router();
const Market = require("../models/market.model");

router.get("/", async function (req, res, next) {
  try {
    const markets = await Market.find();
    res.json(markets);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get("/:market_id", async function (req, res, next) {
  try {
    const market = await Market.findById(req.params.market_id);
    res.json(market);
  } catch (err) {
    res.json({ message: err });
  }
});

router.post("/", async function (req, res, next) {
  try {
    const createdMarket = await Market.create(req.body);
    res.json(createdMarket);
  } catch (err) {
    res.json({ message: err });
  }
});

router.put("/", async function (req, res, next) {
  try {
    const updatedMarket = await Market.findOneAndUpdate(
      { _id: req.body._id },
      { $set: req.body },
      { new: true, useFindAndModify: false }
    );

    res.json(updatedMarket);
  } catch (err) {
    res.json({ message: err });
  }
});

router.delete("/:market_id", async function (req, res, next) {
  try {
    const removedMarket = await Market.remove({ _id: req.params.market_id });
    res.json(removedMarket);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
