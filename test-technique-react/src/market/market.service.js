import axios from "axios";
import { API_URL } from "../config.js";

class MarketService {
  getMarkets = () => {
    return new Promise((resolve, reject) => {
      axios
        .get(API_URL + `markets`)
        .then((res) => {
          resolve(res.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getMarket = (market_id) => {
    return new Promise((resolve, reject) => {
      axios
        .get(API_URL + `markets/` + market_id)
        .then((res) => {
          resolve(res.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  add = (market) => {
    return new Promise((resolve, reject) => {
      axios
        .post(API_URL + `markets`, market)
        .then((res) => {
          resolve(res.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  update = (market) => {
    return new Promise((resolve, reject) => {
      axios
        .put(API_URL + `markets`, market)
        .then((res) => {
          resolve(res.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  delete = (market_id) => {
    return new Promise((resolve, reject) => {
      axios
        .delete(API_URL + `markets/` + market_id)
        .then((res) => {
          resolve(res.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default new MarketService();
