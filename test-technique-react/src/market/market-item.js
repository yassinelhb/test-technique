import { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import service from "./market.service";
import MarketAdd from "./market-add";

function MarketItem({ market, handleMarket, marketDeleted }) {
  const [modal, setModal] = useState(false);

  const updateMarket = (event) => {
    if (event) handleMarket(event);
    setModal(false);
  };

  const deleteMarket = () => {
    service.delete(market._id).then(() => marketDeleted(market));
  };

  return (
    <>
      <div className="market-item" key={market._id}>
        <Dropdown alignRight className="dropdown">
          <Dropdown.Toggle className="dropdownToggle">
            <i className="fa fa-cog"></i>
          </Dropdown.Toggle>

          <Dropdown.Menu className="dropdownMenu">
            <Dropdown.Item onClick={() => setModal(true)}>
              <span className="dropMenuIcon">
                <svg
                  width={18}
                  height={18}
                  viewBox="0 0 18 18"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0 14.2475V17.9975H3.75L14.815 6.93248L11.065 3.18248L0 14.2475Z"
                    fill="#172B49"
                  />
                  <path
                    d="M17.705 2.62748L15.37 0.2925C14.98 -0.0975 14.345 -0.0975 13.955 0.2925L12.125 2.1225L15.875 5.8725L17.705 4.0425C18.095 3.6525 18.095 3.01748 17.705 2.62748Z"
                    fill="#172B49"
                  />
                </svg>
              </span>
              Modifier
            </Dropdown.Item>
            <Dropdown.Item onClick={deleteMarket}>
              <span className="dropMenuIcon">
                <svg
                  width={20}
                  height={21}
                  viewBox="0 0 20 21"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M14.0824 7.23542L12.4055 7.17517L12.0419 16.9006L13.7188 16.9606L14.0824 7.23542Z"
                    fill="#172B49"
                  />
                  <rect
                    x="9.16077"
                    y="7.20529"
                    width="1.67799"
                    height="9.72544"
                    fill="#172B49"
                  />
                  <path
                    d="M7.95733 16.9001L7.59377 7.17468L5.91687 7.23497L6.28047 16.9604L7.95733 16.9001Z"
                    fill="#172B49"
                  />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M0.015625 4.79426V3.15054H19.9839V4.79426H18.2072L16.82 20.235C16.7823 20.6593 16.4193 20.985 15.9844 20.985H3.98688C3.55203 20.985 3.18927 20.6596 3.15123 20.2352L1.76412 4.79426H0.015625ZM4.75516 19.3417H15.2159L16.5228 4.79463H3.44855L4.75516 19.3417Z"
                    fill="#172B49"
                  />
                  <path
                    d="M12.7405 0H7.25901C6.48797 0 5.86069 0.614467 5.86069 1.36976V3.97235H7.53868V1.64372H12.4609V3.97235H14.1388V1.36976C14.1388 0.614467 13.5116 0 12.7405 0Z"
                    fill="#172B49"
                  />
                </svg>
              </span>
              Supprimer définitivement
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <div className="market-item-img">
          <img src={market.image} className="w-100" />
        </div>
        <div className="market-item-info">
          <p className="market-info-desc">{market.title}</p>
          <p className="market-info-price">
            <span className="text-euro">€</span>
            {market.price} €
          </p>
        </div>
      </div>

      {modal && (
        <MarketAdd currentMarket={market} handleMarket={updateMarket} />
      )}
    </>
  );
}

export default MarketItem;
