import { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { marketValidator } from "../validateForm";
import service from "./market.service";

function MarketAdd({ currentMarket, handleMarket }) {
  const [market, setMarket] = useState(currentMarket);
  const [errors, setErrors] = useState({});
  const handleClose = () => {
    handleMarket();
  };

  const handleChange = (e) => {
    setMarket({
      ...market,
      [e.target.name]: e.target.value,
    });

    setErrors({
      ...errors,
      [e.target.name]: false,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const errors = marketValidator(market);
    setErrors(errors);

    if (Object.keys(errors).length === 0) {
      if (currentMarket?._id) {
        service.update(market).then((res) => handleMarket(res));
      } else {
        service.add(market).then((res) => handleMarket(res));
      }
    }
  };

  return (
    <Modal show={true} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>
          {currentMarket?._id ? "Update Market" : "Add Market"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <div className="form-group">
            <label for="title">Title</label>
            <input
              type="text"
              className={
                "form-control " + (errors.title ? "border-danger" : "")
              }
              placeholder="Entre title"
              id="title"
              name="title"
              onChange={handleChange}
              defaultValue={market?.title}
            />
          </div>
          <div className="form-group">
            <label for="price">Price</label>
            <input
              type="number"
              className={
                "form-control " + (errors.price ? "border-danger" : "")
              }
              placeholder="Entre price"
              id="price"
              name="price"
              defaultValue={market?.price}
              onChange={handleChange}
            />
          </div>
          <div className="form-group">
            <label for="image">Image</label>
            <input
              type="text"
              id="image"
              placeholder="Enter image"
              name="image"
              className={
                "form-control " + (errors.image ? "border-danger" : "")
              }
              onChange={handleChange}
              defaultValue={market?.image}
              className="form-control"
            />
            {/*<div className="uploadzone">
              <span className="btn-upload">
                <i className="fa fa-cloud-upload"></i>
              </span>
              <input type="file" />
            </div>*/}
          </div>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={handleSubmit}>
          Save
        </Button>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
export default MarketAdd;
