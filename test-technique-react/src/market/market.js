import { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import service from "./market.service";
import MarketItem from "./market-item";
import MarketAdd from "./market-add";

function Market() {
  const [modal, setModal] = useState(false);
  const [marketList, setMarketList] = useState([]);
  useEffect(() => {
    service.getMarkets().then((res) => setMarketList(res));
  }, []);

  const markets =
    marketList &&
    marketList.map((market) => {
      return (
        <MarketItem
          key={market._id}
          market={market}
          handleMarket={(market) => marketChange(market)}
          marketDeleted={() => deteleMarket(market)}
        />
      );
    });

  const marketChange = (market) => {
    let array = [...marketList];
    const index = array.findIndex((m) => m._id === market._id);
    array[index] = market;
    setMarketList(array);
  };

  const deteleMarket = (market) => {
    setMarketList(marketList.filter((m) => m._id !== market._id));
  };

  const addMarket = (market) => {
    if (market) setMarketList([...marketList, market]);
    setModal(false);
  };

  return (
    <>
      <div className="market-content">
        {markets}
        <div
          className="market-item market-item-add"
          onClick={() => setModal(true)}
        >
          <span className="text-add">+ Créer une nouvelle offre</span>
        </div>
      </div>
      {modal && <MarketAdd handleMarket={addMarket} />}
    </>
  );
}

export default Market;
