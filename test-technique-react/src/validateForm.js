export const marketValidator = (values) => {
    let errors = {};

    if (!values.title || !values.title.trim()) {
        errors.title = 'Title required';
    }

    if (!values.price) {
        errors.price = 'Price is required';
    }

    if (!values.image || !values.image.trim()) {
        errors.image = 'Image is required';
    }

    return errors;
}
